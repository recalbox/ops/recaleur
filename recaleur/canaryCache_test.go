package recaleur

import (
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

func TestWhenAskingForUnexistingCacheEntryThenReturnsFalse(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req.Header.Set("X-Forwarded-For", "10.10.21.10")
	_, found := canaryCache.get(req)
	assert.False(t, found)
}

func TestWhenCanaryEnabledAndSettingNewEntryThenReturnsTrue(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req.Header.Set("X-Forwarded-For", "10.10.21.10")
	assert.True(t, canaryCache.put(req, "CACHEVALUE"))
}

func TestWhenCanaryEnabledButNoHeaderThenDoNotCache(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	assert.False(t, canaryCache.put(req, "CACHEVALUE"))
	_, found := canaryCache.get(req)
	assert.False(t, found)
}

func TestWhenSettingNewEntryButAlreadyExistingThenReturnsTrue(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req.Header.Set("X-Forwarded-For", "10.10.21.10")
	assert.True(t, canaryCache.put(req, "CACHEVALUE"))
	assert.True(t, canaryCache.put(req, "CACHEVALUE"))
}

func TestWhenAskingForExisitingEntryThenReturnValue(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req.Header.Set("X-Forwarded-For", "10.10.21.10")
	assert.True(t, canaryCache.put(req, "CACHEVALUE"))
	value, found := canaryCache.get(req)
	assert.True(t, found)
	assert.Equal(t, "CACHEVALUE", value)
}

func TestWhenAskingForUnExisitingEntryThenReturnFalse(t *testing.T) {
	canaryCache := NewCanaryCache(true, "X-Forwarded-For")
	req := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req.Header.Set("X-Forwarded-For", "10.10.21.10")
	req2 := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf9", nil)
	req2.Header.Set("X-Forwarded-For", "10.10.21.11")
	assert.True(t, canaryCache.put(req, "CACHEVALUE"))
	_, found := canaryCache.get(req)
	assert.True(t, found)
	_, found2 := canaryCache.get(req2)
	assert.False(t, found2)
}
