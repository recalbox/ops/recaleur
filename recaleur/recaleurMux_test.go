package recaleur

import (
	"github.com/stretchr/testify/assert"
	"net/http/httptest"
	"testing"
)

func TestWhenRecaleurMuxCalledWithPathThenAttachHandlerToPath(t *testing.T) {
	path := "/"
	handler := NewCanaryHandler(path, NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, ""))
	mux, _ := NewRecaleurMux(path, handler)
	responseRecorder := httptest.NewRecorder()

	mux.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd23", nil))
	assert.Equal(t, "https://master", responseRecorder.Body.String())
}

func TestWhenRecaleurMuxCalledWithRootPathThenDoNotAnswerOnAllPath(t *testing.T) {
	path := "/"
	handler := NewCanaryHandler(path, NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, ""))
	mux, _ := NewRecaleurMux(path, handler)
	responseRecorder := httptest.NewRecorder()

	mux.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/anotherpath?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd23", nil))
	assert.Equal(t, 404, responseRecorder.Result().StatusCode)
}

func TestHealthcheckAvailable(t *testing.T) {
	path := "/v1/upgrade"
	mux, _ := NewRecaleurMux(path, NewCanaryHandler(path, NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, "")))
	responseRecorder := httptest.NewRecorder()

	mux.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/healthcheck", nil))
	assert.Equal(t, "ok", responseRecorder.Body.String())
}

func TestWhenRecaleurMuxCalledWithWildcardPathThenAnswerOnAllPath(t *testing.T) {
	path := "*"
	handler := NewCanaryHandler(path, NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, ""))
	mux, _ := NewRecaleurMux(path, handler)
	responseRecorder := httptest.NewRecorder()

	mux.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/anotherpath?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd23", nil))
	assert.Equal(t, "https://master", responseRecorder.Body.String())
}
