package recaleur

import (
	"flag"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestWhenParsingCommandLineWithResponseMaster8443ThenReturnsAConfigWithVersionUrl(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://master:8443")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal("https://master:8443", config.Response)
}

func TestWhenParsingCommandLineWithesponseThenReturnsAConfigWithResponse(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal("https://review", config.Response)
}

func TestWhenParsingCommandLineWithNoCanaryThenReturnsAConfigWithCanary256(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal(uint(256), config.Canary)
}

func TestWhenParsingCommandLineWithResponseAndCanaryThenReturnsAConfigWithResponseAndCanary(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal("https://review", config.Response)
	assrt.Equal(uint(50), config.Canary)
}

func TestWhenParsingCommandLineWithoutReviewUrlThenReturnsError(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "")
	_, err := NewConfigFromCommandLine()
	assrt.NotNil(err)
}

func TestWhenParsingCommandLineWithoutExpireThenUsesEmptyValue(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	config, _ := NewConfigFromCommandLine()
	assrt.Nil(config.Expire)
}

func TestWhenParsingCommandLineWithExpireThenCreateTime(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	config, _ := NewConfigFromCommandLine()
	expected, _ := time.Parse("2006-01-02-15:04", expire)
	assrt.Equal(expected, *config.Expire)
}

func TestWhenParsingCommandLineWithExpireInvalidThenReturnError(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-120-12:12")
	_, err := NewConfigFromCommandLine()
	assrt.NotNil(err)
}

func TestWhenParsingCommandLineWithoutResponseDeltaThenResponseDeltaIs0(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal(time.Duration(0), config.Delta)
}

func TestWhenParsingCommandLineWithResponseDeltaThenResponseDeltaInConfig(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal(time.Millisecond*100, config.Delta)
}

func TestWhenParsingCommandLineWithInvalidResponseDeltaThenReturnError(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "hidude")
	_, err := NewConfigFromCommandLine()
	assrt.NotNil(err)
}

func TestWhenParsingCommandLineWithNoPathSPEcifiedThenReturnsRoot(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal("/", config.Path)
}

func TestWhenParsingCommandLineWithInvalidPathThenReturnError(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	flag.Set("path", ":notgoodpath")

	_, err := NewConfigFromCommandLine()
	assrt.NotNil(err)
}

func TestWhenParsingCommandLineWithEmptyPathThenReturnsRoot(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	flag.Set("path", "")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal("/", config.Path)
}

func TestWhenParsingCommandLineWithPathToEncoreThenReturnsEncodedPath(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	flag.Set("path", "/wtf /is this/pàth")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal("/wtf%20/is%20this/p%C3%A0th", config.Path)
}

func TestWhenParsingCommandLineWithWildcardPathThenReturnWildcardPath(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	flag.Set("path", "*")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal("*", config.Path)
}

func TestWhenParsingCommandLineWithoutIgnoreUUIDThenReturnIgnoreUuidFalseInConfig(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal(false, config.IgnoreUUID)
}

func TestWhenParsingCommandLineWithIgnoreUUIDtrueThenReturnIgnoreUuidTrueInConfig(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("canary", "50")
	flag.Set("expire", "2012-12-12-12:12")
	flag.Set("responsedelta", "100ms")
	flag.Set("ignore-uuid", "true")

	config, _ := NewConfigFromCommandLine()
	assrt.Equal(true, config.IgnoreUUID)
}

func TestWhenParsingCommandLineWithRetainIpThenRetainIpIsTrueInConfig(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("retain-ip", "true")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal(true, config.RetainIP)
}

func TestWhenParsingCommandLineWithRetainIpThenRetainIpHeaderIsDefinedThenRetainIpHeaderIsInConfig(t *testing.T) {
	assrt := assert.New(t)
	flag.Set("response", "https://review")
	flag.Set("retain-ip", "true")
	flag.Set("retain-ip-header", "X-Forwarded-For")
	config, _ := NewConfigFromCommandLine()
	assrt.Equal("X-Forwarded-For", config.RetainIPHeader)
}
