package recaleur

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"net/http"
)

// NewCanaryHandler creates a http.HandlerFunc that must be associated with the
// given path by the router
func NewCanaryHandler(path string, canary Canary, cache CanaryCache) http.HandlerFunc {
	return func(output http.ResponseWriter, request *http.Request) {
		if path != "*" && request.URL.Path != path {
			logrus.Warnf("Unacceptable path: %s", path)
			output.WriteHeader(http.StatusNotFound)
		}
		fields := queryParamsToFields(request)
		var uuid string
		if canary.ignoreuuid {
			uuid = ""
		} else {
			if _, ok := fields["uuid"]; !ok {
				logrus.Warnf("Bad Request: %s", errors.New("empty uuid"))
				output.WriteHeader(http.StatusBadRequest)
				return
			}
			uuid = fields["uuid"].(string)
		}

		cacheValue, found := cache.get(request)
		if found {
			logrus.WithFields(fields).Warnf("Canary passed using cache")
			sendResponse(output, http.StatusOK, cacheValue)
		} else {
			response, err := canary.canIGetAnUpdate(uuid)
			if err != nil {
				logrus.WithFields(fields).Warnf("Canary error %f", err)
				sendResponse(output, http.StatusServiceUnavailable, err.Error())
			} else {
				if response.valid {
					cache.put(request, response.content)
					logrus.WithFields(fields).Infof("Canary passed")
					sendResponse(output, http.StatusOK, response.content)
				} else {
					logrus.WithFields(fields).Infof("Canary recaled")
					sendResponse(output, http.StatusServiceUnavailable, "")
				}
			}
		}

	}
}

func sendResponse(output http.ResponseWriter, httpStatus int, body string) {
	output.WriteHeader(httpStatus)
	fmt.Fprint(output, body)
}

func queryParamsToFields(request *http.Request) map[string]interface{} {
	vars := request.URL.Query()
	result := make(map[string]interface{})
	for key, value := range vars {
		result[key] = value[0]
	}
	return result
}
