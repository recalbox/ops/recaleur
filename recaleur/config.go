package recaleur

import (
	"flag"
	"github.com/pkg/errors"
	"net/url"
	"time"
)

type Config struct {
	Path           string
	Response       string
	Expire         *time.Time
	Canary         uint
	Delta          time.Duration
	IgnoreUUID     bool
	RetainIP       bool
	RetainIPHeader string
}

var response string
var expire string
var canary uint
var delta string
var path string
var ignoreuuid bool
var retainIP bool
var retainIPheader string

func init() {
	flag.StringVar(&response, "response", "", "Response to send when return 200")
	flag.StringVar(&expire, "expire", "", "Expire date in UTC (YYYY-MM-dd-HH:mm)")
	flag.UintVar(&canary, "canary", 256, "Amount of people to switch to canary. From 0 (none) to 256 (all). Default '256'")
	flag.StringVar(&delta, "responsedelta", "0ms", "Time laps between two valid responses. Example: 12ms, 10s, 3m ... 0 will deactivate this feature. Default '0'")
	flag.StringVar(&path, "path", "/", "Http request path to serve. Default '/', use '*' for wildcard")
	flag.BoolVar(&ignoreuuid, "ignore-uuid", false, "Ignore uuid and base canary only on responsedelta. Default 'false'")
	flag.BoolVar(&retainIP, "retain-ip", false, "Retain ip of the client, to serve valid responses once one valid response has been sent. Default 'false'")
	flag.StringVar(&retainIPheader, "retain-ip-header", "", "Header to use when --retain-ip is true. Default none")
}

func NewConfigFromCommandLine() (*Config, error) {
	flag.Parse()
	if response == "" || canary > 256 {
		flag.Usage()
		return nil, errors.New("Program parameters invalid")
	}
	var expireDate *time.Time
	if expire != "" {
		exp, err := time.Parse("2006-01-02-15:04", expire)
		if err != nil {
			flag.Usage()
			return nil, errors.New("--expire date invalid")
		}
		expireDate = &exp
	}
	if path != "*" {
		if path == "" {
			path = "/"
		} else {
			query, err := url.Parse(path)
			if err != nil {
				return nil, errors.New("--path invalid")
			}
			path = query.EscapedPath()
		}
	}
	delta, err := time.ParseDuration(delta)
	if err != nil {
		flag.Usage()
		return nil, errors.New("--responsedelta invalid")
	}

	return &Config{Path: path, Response: response, Canary: canary, Delta: delta, Expire: expireDate, IgnoreUUID: ignoreuuid, RetainIP: retainIP, RetainIPHeader: retainIPheader}, nil
}
