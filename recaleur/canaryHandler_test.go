package recaleur

import (
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestWhenHttpGetWithUUIDAndCanary255ThenReturnsUrl(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 255, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd23&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, "https://master", responseRecorder.Body.String())
}

func TestWhenHttpGetNoUUIDThenReturnsBadRequest(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 255, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade", nil))

	assert.Equal(t, 400, responseRecorder.Result().StatusCode)
}

func TestWhenHttpGetWithUUIDAndCanary0ThenReturnsNotAvailable(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 0, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd00&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, 503, responseRecorder.Result().StatusCode)
}

func TestWhenHttpGetWithInvalidUUIDThenReturnsNotAvailable(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 0, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade?uuid=XX&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, 503, responseRecorder.Result().StatusCode)
}

func TestWhenHttpGetWithUUIDBelowCanaryThenReturnsUrl(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 10, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccd00&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, "https://master", responseRecorder.Body.String())
}

func TestWhenHttpGetWithUUIDAboveCanaryThenReturnsNotAvailable(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 10, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, 503, responseRecorder.Result().StatusCode)
}

func TestGivingPathAndUsingASubPathThenReturns404(t *testing.T) {
	handler := NewCanaryHandler("/v1/upgrade", NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade/subpath?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, 404, responseRecorder.Result().StatusCode)
}

func TestGivingWildcardPathAndAnyPathThenReturnsUrl(t *testing.T) {
	handler := NewCanaryHandler("*", NewCanary("https://master", 256, nil, 0, false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()
	responseRecorder2 := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/v1/upgrade/subpath?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, "https://master", responseRecorder.Body.String())
	handler.ServeHTTP(responseRecorder2, httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8&arch=rpi3&boardversion=1", nil))

	assert.Equal(t, "https://master", responseRecorder2.Body.String())
}

func TestWhenIgnoreUUIDThenReturnUrl(t *testing.T) {
	handler := NewCanaryHandler("*", NewCanary("https://master", 256, nil, 0, true), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/", nil))
	assert.Equal(t, "https://master", responseRecorder.Body.String())
}

func TestWhenCanaryTimeBasedShouldRecalThenRecal(t *testing.T) {
	handler := NewCanaryHandler("/", NewCanary("https://master", 256, nil, time.Duration(10*time.Second), false), NewCanaryCache(false, ""))
	responseRecorder := httptest.NewRecorder()
	responseRecorder2 := httptest.NewRecorder()
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil))
	assert.Equal(t, http.StatusOK, responseRecorder.Code)
	handler.ServeHTTP(responseRecorder2, httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil))
	assert.Equal(t, http.StatusServiceUnavailable, responseRecorder2.Code)
}

func TestWhenEntryNotInCacheAndUUIDShouldPassThenPass(t *testing.T) {
	handler := NewCanaryHandler("/", NewCanary("https://master", 256, nil, 0, true), NewCanaryCache(true, "X-Forwarded-For"))
	responseRecorder := httptest.NewRecorder()

	handler.ServeHTTP(responseRecorder, httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil))
	assert.Equal(t, "https://master", responseRecorder.Body.String())
}

func TestWhenEntryInCacheAndCanaryTimeBasedShouldRecalThenPass(t *testing.T) {
	handler := NewCanaryHandler("/", NewCanary("https://master", 256, nil, time.Duration(10*time.Second), false), NewCanaryCache(true, "X-Forwarded-For"))

	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()

	responseRecorder := httptest.NewRecorder()
	req1 := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req1.Header.Set("X-Forwarded-For", "10.10.21.10")
	handler.ServeHTTP(responseRecorder, req1)
	assert.Equal(t, http.StatusOK, responseRecorder.Code)

	responseRecorder2 := httptest.NewRecorder()
	req2 := httptest.NewRequest("GET", "/?uuid=42998f7f-1a46-4b24-a64d-6e5d0e7ccdf8", nil)
	req2.Header.Set("X-Forwarded-For", "10.10.21.10")
	handler.ServeHTTP(responseRecorder2, req2)
	assert.Equal(t, http.StatusOK, responseRecorder2.Code)
}
