FROM golang:1.8 as build

ARG CGO_ENABLED=0
ARG GOOS=linux
ARG GOARCH=amd64

WORKDIR /go/src/gitlab.com/recalbox/ops/recaleur
COPY . /go/src/gitlab.com/recalbox/ops/recaleur

RUN go get -v -t ./...
RUN go test ./recaleur -cover
RUN go install -a -ldflags '-s'

FROM scratch
EXPOSE 8080

COPY --from=build /go/bin/recaleur /

ENTRYPOINT ["/recaleur"]
