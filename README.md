Recaleur
--------

## Initial purpose

Recaleur is a filter. It prevents recalboxes from starting the update cycle.


## How to use

Using docker you can configure recaleur with:
- `--response` the response to send
- `--canary` the percentage of valid responses (between 0 and 256), 
- `--expire` canary expire date, all request are accepted
- `--responsedelta` delta time between two valid http response
- `--path` path to serve. Default '/'
- `--ignore-uuid` Ignore uuid and base canary only on --responsedelta. If canary == 0, no response would be served.
- `--retain-ip` Cache client ip to always send valid responses after a valid response. Default false
- `--retain-ip-header` Header to use for getting client ip

To serve a correct response to 50% of uuids, with 100 milli seconds between every response:
```bash
docker run -p 8080:8080 registry.gitlab.com/recalbox/ops/recaleur:master \
--response "https://master.recalbox.com" \
--canary 128 \
--responsedelta "100ms" \
--path "/v1/upgrade"
```

To serve a correct response to 50% of uuids, with 100 milli seconds between every response:
```bash
docker run -p 8080:8080 registry.gitlab.com/recalbox/ops/recaleur:master \
--response "https://master.recalbox.com" \
--canary 128 \
--responsedelta "100ms" \
--path "/v1/upgrade"
--retain-ip
--retain-ip-header "X-Forwarded-For"
```

To serve a correct response to 50% of uuids, with 100 milli seconds between every response, being sure that a client having a valid response 
is in cache and will have valid responses for future requests:
```bash
docker run -p 8080:8080 registry.gitlab.com/recalbox/ops/recaleur:master \
--response "https://master.recalbox.com" \
--canary 128 \
--responsedelta "100ms" \
--path "/v1/upgrade"
```

To serve a correct response to 25% of uuids, with 1 second between every response, and serve 100% of uuids from 2017-09-20-22:00, still with 1 second between every response:
```bash
docker run -p 8080:8080 registry.gitlab.com/recalbox/ops/recaleur:master \
--response "https://master.recalbox.com" \
--canary 64 \
--expire "2017-09-20-22:00" \
--responsedelta "1s" \
--path "/v1/upgrade"
```

To serve a correct response to everyone on every path:
```bash
docker run -p 8080:8080 registry.gitlab.com/recalbox/ops/recaleur:master \
--response "https://master.recalbox.com" \
--path "*" \
--ignore-uuid
```

`--expire` will set canary to 256 (answer all request) at the given date, except if canary is set to 0 (no request answered) 


## How to build

with go:
```bash
export GOPATH=~/go
mkdir -p ${GOPATH}/gitlab.com/recalbox/ops/
cd ${GOPATH}/gitlab.com/recalbox/ops/
git clone git@gitlab.com:recalbox/ops/recaleur.git
cd recaleur
go get -v -t ./...
go test ./app -coverprofile cover.out
go tool cover -html=cover.out
go build
```

with docker:
```bash
git clone git@gitlab.com:recalbox/ops/recaleur.git
cd recaleur
docker build -t builder -f build.Dockerfile .
docker run -e "GOOS=your_os" -e "GOARCH=your_architecture" -e "CGO_ENABLED=0" -v `pwd`/dist/:/go/bin/ builder go install -a -ldflags '-s'
```

## How to contribute

Issues and Merge Requests are open.