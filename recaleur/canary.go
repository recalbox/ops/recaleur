package recaleur

import (
	"github.com/pkg/errors"
	"strconv"
	"time"
)

type Canary struct {
	url         string
	canary      uint
	expire      *time.Time
	delta       time.Duration
	lastReponse *time.Time
	ignoreuuid  bool
}

type CanaryResponse struct {
	content string
	valid   bool
}

func (canary *Canary) canIGetAnUpdate(uuid string) (CanaryResponse, error) {
	if canary.deltaTimeAllowsMeToServeResponse() {
		if canary.shouldSendValidResponseBecauseExpiredOrIgnoreUUID() {
			return CanaryResponse{content: canary.url, valid: true}, nil
		}
		return canary.calculateResponseForUUID(uuid)
	} else {
		return CanaryResponse{content: "", valid: false}, nil
	}
}

func (canary *Canary) deltaTimeAllowsMeToServeResponse() bool {
	now := time.Now().UTC()
	if canary.lastReponse == nil {
		canary.lastReponse = &now
		return true
	}
	canServe := canary.lastReponse.Add(canary.delta).Before(now)
	if canServe {
		canary.lastReponse = &now
	}
	return canServe
}

func (canary *Canary) calculateResponseForUUID(uuid string) (CanaryResponse, error) {
	if len(uuid) >= 2 {
		lastByte := uuid[len(uuid)-2:]
		conv, err := strconv.ParseUint(lastByte, 16, 8)
		if err != nil {
			return invalidUUIDError()
		}
		if conv < uint64(canary.canary) {
			return CanaryResponse{content: canary.url, valid: true}, nil
		} else {
			return CanaryResponse{content: "", valid: false}, nil
		}
	}
	return invalidUUIDError()
}

func (canary *Canary) shouldSendValidResponseBecauseExpiredOrIgnoreUUID() bool {
	return (canary.ignoreuuid || canary.expired()) && canary.canary > 0
}

func (canary *Canary) expired() bool {
	return canary.expire != nil && canary.expire.Before(time.Now().UTC())
}

func invalidUUIDError() (CanaryResponse, error) {
	return CanaryResponse{content: "", valid: false}, errors.New("Invalid UUID")
}

// NewCanary creates a canary based on parameters.
// The canary can be used to decide if an uuid should have a response or no.
func NewCanary(url string, canary uint, expire *time.Time, delta time.Duration, ignoreuuid bool) Canary {
	return Canary{url: url, canary: canary, expire: expire, delta: delta, ignoreuuid: ignoreuuid}
}
