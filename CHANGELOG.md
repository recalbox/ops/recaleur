# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [2.4.0] - 2017-08-21
### Added
- `-retain-ip` and `-retain-ip-header` allow client identification by ip, and response cache

## [2.3.0] - 2017-08-19
### Added
- `-path` now supports wildcard ('*')
- `-ignore-uuid` disable uuid check, so recaleur use only responsedelta

## [2.2.0] - 2017-08-19
### Added
- `-responsedelta` application parameter configure the time between two valid answers
- `-path` application parameter configure the path to use for http requests

## [2.1.0] - 2017-08-15
### Added
- `-expire` application parameter configure the date when all request must be served (canary disabled) 
- `-canary` application parameter configure the percentage of request to answer (using `uuid` query param)
- `-reponse` application parameter configure the response
