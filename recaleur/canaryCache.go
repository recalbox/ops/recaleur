package recaleur

import "net/http"

// CanaryCache struct keeping the state of the cache
type CanaryCache struct {
	cache          map[string]string
	retainIP       bool
	retainIPHeader string
}

func (cache CanaryCache) get(request *http.Request) (string, bool) {
	if cache.retainIP {
		if cache.retainIPHeader != "" {
			head := request.Header.Get(cache.retainIPHeader)
			if head != "" {
				if value, ok := cache.cache[head]; ok {
					return value, true
				}
			}
		}
	}
	return "", false
}

func (cache CanaryCache) put(request *http.Request, value string) bool {
	if cache.retainIP {
		if cache.retainIPHeader != "" {
			head := request.Header.Get(cache.retainIPHeader)
			if head != "" {
				cache.cache[head] = value
				return true
			}
		}
	}
	return false
}

// NewCanaryCache creates a new CanaryCache
func NewCanaryCache(cacheRetainIP bool, cacheRetainIPHeader string) CanaryCache {
	return CanaryCache{cache: make(map[string]string), retainIP: cacheRetainIP, retainIPHeader: cacheRetainIPHeader}
}
