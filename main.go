package main

import (
	"github.com/Sirupsen/logrus"
	"gitlab.com/recalbox/ops/recaleur/recaleur"
	"net/http"
)

func main() {
	wrap()
}

func wrap() error {
	config, err := recaleur.NewConfigFromCommandLine()
	if err != nil {
		logrus.Error(err)
		return err
	}

	mux, err := recaleur.NewRecaleurMux(config.Path,
		recaleur.NewCanaryHandler(
			config.Path,
			recaleur.NewCanary(config.Response, config.Canary, config.Expire, config.Delta, config.IgnoreUUID),
			recaleur.NewCanaryCache(config.RetainIP, config.RetainIPHeader)))
	if err != nil {
		logrus.Error(err)
		return err
	}

	http.ListenAndServe(":8080", mux)

	return nil

}
