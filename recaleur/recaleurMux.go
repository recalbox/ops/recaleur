package recaleur

import (
	"fmt"
	"net/http"
)

// NewRecaleurMux creates a Mux that associate the http path to the given handler
// It also create the healthcheck handle
func NewRecaleurMux(canaryPath string, handler http.HandlerFunc) (*http.ServeMux, error) {
	mux := http.NewServeMux()
	if canaryPath == "*" {
		canaryPath = "/"
	}
	mux.Handle(canaryPath, handler)
	mux.HandleFunc("/v1/healthcheck", func(output http.ResponseWriter, request *http.Request) {
		output.WriteHeader(http.StatusOK)
		fmt.Fprint(output, "ok")
	})

	return mux, nil
}
