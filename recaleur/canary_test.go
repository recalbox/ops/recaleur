package recaleur

import (
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestWhenRequestingUrlAndCanary256ThenReturnsUrl(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 256, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("123456")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenCanary32AndUUIDLastByteEquals32ThenDoNotReturnAddress(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 32, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c097116e-9c0f-4a90-83ab-79037e443220")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
}

func TestWhenCanary32AndUUIDLastByteAbove32ThenDoNotReturnAddress(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 32, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c097116e-9c0f-4a90-83ab-79037e443221")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
}

func TestWhenCanary32AndUUIDLastByteBelow32ThenPass(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 32, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c097116e-9c0f-4a90-83ab-79037e44321f")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenCanary256AndUUIDMax255ThenPass(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 256, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c097116e-9c0f-4a90-83ab-79037e4432ff")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenUUIDNotHexaThenError(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 32, nil, time.Duration(0), false)
	_, err := canary.canIGetAnUpdate("xx")
	assert.NotNil(err)
}

func TestWhenUUIDaaNotHexaThenNoError(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 256, nil, time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("aa")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func date(date string) time.Time {
	res, _ := time.Parse("2006-01-02-15:04:05", date)
	return res
}

func datep(date string) *time.Time {
	res, _ := time.Parse("2006-01-02-15:04:05", date)
	return &res
}

func TestWhenExpireDateAfterNowAndCanary256ThenPass(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, datep("2018-01-02-15:05:00"), time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("aa")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenExpireDateAfterNowAndCanaryMustRecalThenRecal(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 1, datep("2018-01-02-15:05:00"), time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
}

func TestWhenExpireDateBeforeNowAndCanaryMustRecalThenPassAll(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 1, datep("2018-01-02-15:03:00"), time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenExpireDateBeforeNowAndCanary0ThenRecal(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 0, datep("2018-01-02-15:03:00"), time.Duration(0), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
}

func TestWhenDeltaIsOneSecondThenDoNotServeTwoResponsesInTheSameSecond(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, nil, time.Duration(1*time.Second), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse2)
}

func TestWhenDeltaIsOneSecondButUUIDnotInCanaryThenAllResponsesRecaled(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 0, nil, time.Duration(1*time.Second), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse2)
}

func TestWhenDeltaIs1000AndTwoRequestMadeAtOneSecondIntervalThenPass(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, nil, time.Duration(1*time.Second), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
	patch2 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:02") })
	defer patch2.Unpatch()
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse2)
}

func TestWhenDeltaIsTwoSecondsAndTwoRequestMadeAtOneSecondIntervalThenRecalTheSecondRequest(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, nil, time.Duration(2*time.Second), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
	patch2 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:01") })
	defer patch2.Unpatch()
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse2)
}

func TestWhenDeltaIsTwoSecondsAndTwoRequestMadeAtTwoSecondIntervalThenPassButRecalAThirdOneSecondAfter(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, nil, time.Duration(2*time.Second), false)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
	patch2 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:03") })
	defer patch2.Unpatch()
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse2)
	patch3 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:04") })
	defer patch3.Unpatch()
	canaryResponse3, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse3)
}

func TestWhenDeltaIsThreeSecondsAndARequestIsRecaledThenDoServeNextRequestWithDeltaofThePreviouslyValidRequest(t *testing.T) {
	assert := assert.New(t)
	patch := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:00") })
	defer patch.Unpatch()
	canary := NewCanary("https://master", 256, nil, time.Duration(3*time.Second), true)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
	patch2 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:02") })
	defer patch2.Unpatch()
	canaryResponse2, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse2)
	patch3 := monkey.Patch(time.Now, func() time.Time { return date("2018-01-02-15:04:04") })
	defer patch3.Unpatch()
	canaryResponse3, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse3)
}

func TestWhenIgnoreUUIDAndCanaryAboveZeroThenAlwaysReturnResponse(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 1, nil, time.Duration(0), true)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"https://master", true}, canaryResponse)
}

func TestWhenIgnoreUUIDAndCanaryZeroThenRecal(t *testing.T) {
	assert := assert.New(t)
	canary := NewCanary("https://master", 0, nil, time.Duration(0), true)
	canaryResponse, _ := canary.canIGetAnUpdate("c0febabe")
	assert.Equal(CanaryResponse{"", false}, canaryResponse)
}
